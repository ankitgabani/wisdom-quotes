//
//  HomeSliderCollectionCell.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 09/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class HomeSliderCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lblNam: UILabel!
    @IBOutlet weak var mainVIew: UIView!
    
    
}
