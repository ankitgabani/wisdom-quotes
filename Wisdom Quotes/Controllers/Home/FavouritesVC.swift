//
//  FavouritesVC.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 09/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class FavouritesVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
   
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
