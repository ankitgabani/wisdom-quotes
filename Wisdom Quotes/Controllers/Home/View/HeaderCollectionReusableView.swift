//
//  HeaderCollectionReusableView.swift
//  Leap
//
//  Created by Mac MIni M1 on 14/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class HeaderCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var collectionViewSlider : UICollectionView!
    @IBOutlet weak var pageControl : UIPageControl!

}
