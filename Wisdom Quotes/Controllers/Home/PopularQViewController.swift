//
//  PopularQViewController.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 14/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class PopularQViewController: UIViewController {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoData: UILabel!
    
    var objTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if objTitle == "Popular\nQuotes" {
            self.lblTitle.text = "Popular Quotes"
            self.lblNoData.text = "No Popular Quotes found."
        }
        else if objTitle == "Top\nQuotes" {
            self.lblTitle.text = "Top Quotes"
            self.lblNoData.text = "No Top Quotes found."
        }
        else if objTitle == "Short\nQuotes" {
            self.lblTitle.text = "Short Quotes"
            self.lblNoData.text = "No Short Quotes found."
        }
        else if objTitle == "Proverbs" {
            self.lblTitle.text = "Proverbs Quotes"
            self.lblNoData.text = "No Proverbst Quotes found."
        }
        else if objTitle == "Latest\nQuotes" {
            self.lblTitle.text = "Latest Quotes"
            self.lblNoData.text = "No Latest Quotes found."
        }
        else if objTitle == "Random\nQuotes" {
            self.lblTitle.text = "Random Quotes"
            self.lblNoData.text = "No Random Quotes found."
        }
        else if objTitle == "Random\nQuotes" {
            self.lblTitle.text = "Random Quotes"
            self.lblNoData.text = "No Random Quotes found."
        }
        
        // Do any additional setup after loading the view.
    }
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
