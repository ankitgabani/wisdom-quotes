//
//  HomeVC.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 09/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    @IBOutlet weak var naviView: UIView!
    
    var pageControll: UIPageControl!
    @IBOutlet weak var lblTitle: UILabel!
    
    var collectionViewSlider: UICollectionView!
    
    @IBOutlet weak var collectionViewCategory: UICollectionView!
    
    var arrImage = ["bg6","bg2","bg12","bg6","bg5"]
    var arrNameSlider = ["A thought is a Cosmic Order waiting to happen.","Without a sense of urgency, desire loses its value.","A thought is a Cosmic Order waiting to happen.","Without a sense of urgency, desire loses its value.","What you think of yourself is much more important than what people think of you."]
    
    var arrImageList = ["ictext","icpopular","ictop","icshort","icproverbs","icfavourites","icpopular","icrandom","icfavouritepic"]
    
    var arrNameCategory = ["Text\nQuotes","Popular\nQuotes","Top\nQuotes","Short\nQuotes","Proverbs","Favourites","Latest\nQuotes","Random\nQuotes","Favourite\npicture\nQuotes"]
    
    var timerRR: Timer?

    
    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 0.0,
                                     bottom: 0.0,
                                     right: 0.0)
    let itemsPerRow: CGFloat = 1
    
    
    
    let sectionInsets1 = UIEdgeInsets(top: 0.0,
                                      left: 20.0,
                                      bottom: 0.0,
                                      right: 20.0)
    let itemsPerRow1: CGFloat = 3
    
    var flowLayout1: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                  let height = (self.view.frame.size.height - 270 - 20*4)/3
                 _flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 80)/3, height: height)

            case 1334:
                print("iPhone 6/6S/7/8")
                  let height = (self.view.frame.size.height - 270 - 20*4)/3
                 _flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 80)/3, height: height)

            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                 let height = (self.view.frame.size.height - 270 - 20*4)/3
                _flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 80)/3, height: height)

            case 2436:
                print("iPhone X/XS/11 Pro")
                 let height = (self.view.frame.size.height - 310 - 20*4)/3
                _flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 80)/3, height: height)

            case 2688:
                print("iPhone XS Max/11 Pro Max")
                 let height = (self.view.frame.size.height - 310 - 20*4)/3
                _flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 80)/3, height: height)

            case 1792:
                print("iPhone XR/ 11 ")
                 let height = (self.view.frame.size.height - 310 - 20*4)/3
                _flowLayout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 80)/3, height: height)

            default:
                print("Unknown")
            }
        }
        
            _flowLayout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 0, right: 20)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        
        // edit properties here
        
        return _flowLayout
    }
    
    // MARK: - View cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myNib = UINib(nibName: "HeaderCollectionReusableView",bundle: nil)
        collectionViewCategory.register(myNib, forSupplementaryViewOfKind:UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerView")
        
        // collectionViewSlider.collectionViewLayout = flowLayout
        collectionViewCategory.collectionViewLayout = flowLayout1
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        timerRR?.invalidate()
         timerRR = nil
        self.collectionViewCategory.reloadData()
    }
    
}

extension HomeVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewSlider
        {
            return arrImage.count
        }
        else
        {
            return 9
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView .tag == 1001
        {
            let cell = collectionViewSlider.dequeueReusableCell(withReuseIdentifier: "HomeSliderCollectionCell", for: indexPath) as!  HomeSliderCollectionCell
            
            cell.imgBg.image = UIImage(named: arrImage[indexPath.row])
            cell.lblNam.text = arrNameSlider[indexPath.row]
            
            DispatchQueue.main.async {
                cell.imgBg.layer.cornerRadius = 12
            }
            
            return cell
        }
        else
        {
            let cell = collectionViewCategory.dequeueReusableCell(withReuseIdentifier: "HomeCategoryCollectionCell", for: indexPath) as!  HomeCategoryCollectionCell
            
            cell.imgBg.image = UIImage(named: arrImageList[indexPath.row])
            cell.lblNam.text = arrNameCategory[indexPath.row]
            
            cell.mainView.backgroundColor = .random
            
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerView", for: indexPath) as! HeaderCollectionReusableView
        
        headerView.collectionViewSlider.delegate = self
        headerView.collectionViewSlider.dataSource = self
        
        headerView.collectionViewSlider.tag = 1001
        
        self.collectionViewSlider = headerView.collectionViewSlider
        
        self.collectionViewSlider.register(UINib.init(nibName: "HomeSliderCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeSliderCollectionCell")
        
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: self.collectionViewSlider.frame.size.width, height: self.collectionViewSlider.frame.size.height)
        }
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 0.0
        
        self.collectionViewSlider.collectionViewLayout = _flowLayout
        self.pageControll = headerView.pageControl
        self.pageControll.numberOfPages = self.arrImage.count
        
        self.collectionViewSlider.reloadData()
        
        self.startTimer()
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        if collectionView == collectionViewCategory{
            return CGSize.init(width: 414, height: 200)
         }
        else{
            return CGSize.init(width: 0, height: 0)
         }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let page = scrollView.contentOffset.x/scrollView.frame.size.width
        self.pageControll.currentPage = (Int)(page)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionViewCategory {
            
            if indexPath.row == 0 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else if indexPath.row == 1 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopularQViewController") as! PopularQViewController
                vc.objTitle = arrNameCategory[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else if indexPath.row == 2 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopularQViewController") as! PopularQViewController
                vc.objTitle = arrNameCategory[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 3 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopularQViewController") as! PopularQViewController
                vc.objTitle = arrNameCategory[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 4 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopularQViewController") as! PopularQViewController
                vc.objTitle = arrNameCategory[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 5 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FavouritesVC") as! FavouritesVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 6 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PopularQViewController") as! PopularQViewController
                vc.objTitle = arrNameCategory[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 7 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ListOptionViewController") as! ListOptionViewController
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else if indexPath.row == 8 {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FavouritesVC") as! FavouritesVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    /**
     Scroll to Next Cell
     */
    @objc func scrollToNextCell(){
        
        if pageControll.currentPage == pageControll.numberOfPages - 1 {
            pageControll.currentPage = 0
        } else {
            pageControll.currentPage += 1
        }
        
        collectionViewSlider.scrollToItem(at: IndexPath(row: pageControll.currentPage, section: 0), at: .right, animated: true)
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func startTimer() {
        
        timerRR = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
