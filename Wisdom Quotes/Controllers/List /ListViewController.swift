//
//  ListViewController.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 10/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Toast_Swift
class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrImg = ["bg3","bg2","bg4","bg5","bg6","bg7","bg8","bg10","bg12","bg13","bg15","bg16"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ListTabTableCell") as! ListTabTableCell
        
        cell.imgBg.clipsToBounds = true
        cell.imgBg.layer.cornerRadius = 5
        cell.imgBg.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        cell.viewBlack.clipsToBounds = true
        cell.viewBlack.layer.cornerRadius = 5
        cell.viewBlack.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        cell.imgBg.image = UIImage(named: arrImg[indexPath.row])
        
        cell.viewBottom.clipsToBounds = true
        cell.viewBottom.layer.cornerRadius = 5
        cell.viewBottom.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        
        cell.btnDownloadn.tag = indexPath.row
        cell.btnDownloadn.addTarget(self, action: #selector(clickedDownload(sender:)), for: .touchUpInside)
        
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(clickedShare(sender:)), for: .touchUpInside)

        cell.btnCopy.tag = indexPath.row
        cell.btnCopy.addTarget(self, action: #selector(clickedCopy(sender:)), for: .touchUpInside)

        return cell
    }
    
    @objc func clickedDownload(sender: UIButton) {
        
        let index = IndexPath(row: sender.tag, section: 0)
        let cell = tblView.cellForRow(at: index) as? ListTabTableCell
        
        UIImageWriteToSavedPhotosAlbum((cell?.viewScreenShot.makeSnapshot())!, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func clickedCopy(sender: UIButton) {
        
        let index = IndexPath(row: sender.tag, section: 0)
        let cell = tblView.cellForRow(at: index) as? ListTabTableCell
        
        UIPasteboard.general.string = cell?.lblMsg.text
        self.view.makeToast("Copy!")
    }
    
    @objc func clickedShare(sender: UIButton) {
        
        let index = IndexPath(row: sender.tag, section: 0)
        let cell = tblView.cellForRow(at: index) as? ListTabTableCell
        
        let vc = UIActivityViewController(activityItems: [cell?.viewScreenShot.makeSnapshot()!], applicationActivities: nil)
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = vc.popoverPresentationController {
                let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                let barButton = UIBarButtonItem(customView: btnShare)
                popoverController.barButtonItem = barButton
            }
        }
        present(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "QSEditorVC") as! QSEditorVC
        vc.objImage = arrImg[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 510
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
}


extension CALayer {
    func makeSnapshot() -> UIImage? {
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(frame.size, false, scale)
        defer { UIGraphicsEndImageContext() }
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        render(in: context)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        
        return screenshot
    }
}

extension UIView {
    func makeSnapshot() -> UIImage? {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: frame.size)
            return renderer.image { _ in drawHierarchy(in: bounds, afterScreenUpdates: true) }
        } else {
            return layer.makeSnapshot()
        }
    }
}
