//
//  ListOptionViewController.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 11/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ListOptionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrImg = ["LO1","LO2","LO4","LO5","LO6","LO7","LO8","LO10"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrImg.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "ListOptionCell") as! ListOptionCell
        
        cell.imgBg.clipsToBounds = true
        cell.imgBg.layer.cornerRadius = 5
        cell.imgBg.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        cell.viewBlack.clipsToBounds = true
        cell.viewBlack.layer.cornerRadius = 5
        cell.viewBlack.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top Corner
        
        cell.imgBg.image = UIImage(named: arrImg[indexPath.row])
        
        cell.viewBottom.clipsToBounds = true
        cell.viewBottom.layer.cornerRadius = 5
        cell.viewBottom.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner] // Top Corner
        
        
        cell.btnDownloand.tag = indexPath.row
        cell.btnDownloand.addTarget(self, action: #selector(clickedDownload(sender:)), for: .touchUpInside)
        
        cell.btnShare.tag = indexPath.row
        cell.btnShare.addTarget(self, action: #selector(clickedShare(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    
    @objc func clickedDownload(sender: UIButton) {
        
        let index = IndexPath(row: sender.tag, section: 0)
        let cell = tblView.cellForRow(at: index) as? ListOptionCell
        
        UIImageWriteToSavedPhotosAlbum((cell?.imgBg.image!)!, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func clickedShare(sender: UIButton) {
        
        let index = IndexPath(row: sender.tag, section: 0)
        let cell = tblView.cellForRow(at: index) as? ListOptionCell
        
        let vc = UIActivityViewController(activityItems: [cell?.imgBg.image!], applicationActivities: nil)
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = vc.popoverPresentationController {
                let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                let barButton = UIBarButtonItem(customView: btnShare)
                popoverController.barButtonItem = barButton
            }
        }
        present(vc, animated: true)
    }
    
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ListOptionCatagryVC") as! ListOptionCatagryVC
        vc.arrImageList = arrImg
        vc.cuurentIndex = indexPath.row
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 510
    }
    
    
}
