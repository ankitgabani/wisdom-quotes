//
//  ListTabTableCell.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 10/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ListTabTableCell: UITableViewCell {

    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var viewBlack: UIView!
    
    @IBOutlet weak var btnColor: UIButton!
    @IBOutlet weak var btnCopy: UIButton!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnDownloadn: UIButton!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var viewScreenShot: UIView!
    @IBOutlet weak var lblMsg: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
