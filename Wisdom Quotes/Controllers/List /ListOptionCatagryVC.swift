//
//  ListOptionCatagryVC.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 11/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ListOptionCatagryVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var collectionViewList: UICollectionView!
    
    var arrImageList = [""]
    
    var cuurentIndex = 0
    
    var currentImage = UIImage()
    
    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 0.0,
                                     bottom: 0.0,
                                     right: 0.0)
    let itemsPerRow: CGFloat = 1
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: self.collectionViewList.frame.size.width, height: self.collectionViewList.frame.size.height)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
            _flowLayout.minimumInteritemSpacing = 0.0
            _flowLayout.minimumLineSpacing = 0.0
            
        }
        
        return _flowLayout
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewList.delegate = self
        collectionViewList.dataSource = self
        
        
        collectionViewList.collectionViewLayout = flowLayout
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: self.cuurentIndex, section: 0)
            self.collectionViewList.scrollToItem(at: indexPath, at: .left, animated: false)
            self.collectionViewList.reloadData()
            
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedBackImage(_ sender: Any) {
        scrollToPreviousOrNextCell(direction: "Previous")
        
    }
    
    @IBAction func clickedFavo(_ sender: Any) {
    }
    
    @IBAction func clickedDownload(_ sender: Any) {
        UIImageWriteToSavedPhotosAlbum(currentImage, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    @IBAction func clickedShare(_ sender: Any) {
        let vc = UIActivityViewController(activityItems: [currentImage], applicationActivities: nil)
        if UIDevice.current.userInterfaceIdiom == .pad {
            if let popoverController = vc.popoverPresentationController {
                let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                let barButton = UIBarButtonItem(customView: btnShare)
                popoverController.barButtonItem = barButton
            }
        }
        present(vc, animated: true)
    }
    
    @IBAction func clickedNextImg(_ sender: Any) {
        scrollToPreviousOrNextCell(direction: "Next")
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListOptionCatagryCollectionCell", for: indexPath) as!  ListOptionCatagryCollectionCell
        
        cell.imgList.image = UIImage(named: arrImageList[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        currentImage = UIImage(named: arrImageList[indexPath.row])!
    }
    
    
    func scrollToPreviousOrNextCell(direction: String) {
        
        DispatchQueue.global(qos: .background).async {
            
            DispatchQueue.main.async {
                
                let firstIndex = 0
                let lastIndex = (self.arrImageList.count) - 1
                
                let visibleIndices = self.collectionViewList.indexPathsForVisibleItems
                
                let nextIndex = visibleIndices[0].row + 1
                let previousIndex = visibleIndices[0].row - 1
                
                let nextIndexPath: IndexPath = IndexPath.init(item: nextIndex, section: 0)
                let previousIndexPath: IndexPath = IndexPath.init(item: previousIndex, section: 0)
                
                if direction == "Previous" {
                    if previousIndex < firstIndex {
                        
                    } else {
                        
                        self.collectionViewList.scrollToItem(at: previousIndexPath, at: .centeredHorizontally, animated: true)
                    }
                } else if direction == "Next" {
                    
                    if nextIndex > lastIndex {
                        
                    } else {
                        
                        self.collectionViewList.scrollToItem(at: nextIndexPath, at: .centeredHorizontally, animated: true)
                    }
                }
            }
        }
    }
}
