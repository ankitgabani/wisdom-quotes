//
//  ChooseBgVC.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 11/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices


class ChooseBgVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var collectionViewBG: UICollectionView!
    
    var imagePicker = UIImagePickerController()
    
    
    var delegate: BGDelegate?
    
    var arrImage = ["bg0","bg1","bg2","bg3","bg4","bg5","bg6","bg7","bg8","bg9","bg10","bg11","bg12","bg13","bg14","bg15"]
    
    let sectionInsets1 = UIEdgeInsets(top: 0.0,
                                      left: 3.0,
                                      bottom: 0.0,
                                      right: 3.0)
    let itemsPerRow1: CGFloat = 3
    
    var flowLayout1: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        
        DispatchQueue.main.async {
            
            let paddingSpace = self.sectionInsets1.left * (self.itemsPerRow1 + 1)
            let availableWidth = UIScreen.main.bounds.size.width - paddingSpace
            let widthPerItem = (availableWidth) / self.itemsPerRow1
            
            _flowLayout.itemSize = CGSize(width: widthPerItem, height: 200)
            
            _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
            _flowLayout.minimumInteritemSpacing = 0
            _flowLayout.minimumLineSpacing = 2
        }
        
        // edit properties here
        
        return _flowLayout
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewBG.collectionViewLayout = flowLayout1
        self.imagePicker.delegate = self
        
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionViewBG.dequeueReusableCell(withReuseIdentifier: "ChooseBgCollectionViewCell", for: indexPath) as!  ChooseBgCollectionViewCell
        
        cell.imgBG.image = UIImage(named: arrImage[indexPath.row])
        
        DispatchQueue.main.async {
            cell.imgBG.layer.cornerRadius = 12
            cell.viewBG.layer.cornerRadius = 12
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            choosePicture()
        }
        else
        {
            delegate?.onChangeBG(type: arrImage[indexPath.row], img: UIImage(named: "bg12")!) // Delegate
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func choosePicture() {
        let alert  = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
        alert.modalPresentationStyle = .overCurrentContext
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        let popoverController = alert.popoverPresentationController
        
        popoverController?.permittedArrowDirections = .up
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Camera & Photo Picker
    func openCamera()
    {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.mediaTypes = [kUTTypeImage as String]
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)          }
    }
    
    func openGallary()
    {
        
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = ["public.image"]
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            delegate?.onChangeBG(type: "", img: image) // Delegate
            self.navigationController?.popViewController(animated: true)
            
        }
        else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            delegate?.onChangeBG(type: "", img: image) // Delegate
            self.navigationController?.popViewController(animated: true)
            
        }
        
        picker.dismiss(animated: true, completion: nil)
        
    }
}
