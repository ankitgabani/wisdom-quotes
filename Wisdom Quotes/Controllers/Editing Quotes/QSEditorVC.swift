//
//  QSEditorVC.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 11/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown
import Toast_Swift

//MARK:- Delegate
protocol BGDelegate
{
    func onChangeBG(type: String, img: UIImage)
}

class QSEditorVC: UIViewController, BGDelegate {
    
    @IBOutlet weak var viewSizeChangeR: UIView!
    @IBOutlet weak var lblChnageFontSize: UILabel!
    @IBOutlet weak var btnSize1: UIButton!
    @IBOutlet weak var btnSize0: UIButton!
    
    
    @IBOutlet weak var viewColorChangeR: UIView!
    @IBOutlet weak var collectionViewColor: UICollectionView!
    
    @IBOutlet weak var viewFontChangeR: UIView!
    @IBOutlet weak var collectionViewFonr: UICollectionView!
    
    
    @IBOutlet weak var viewAlignmentChangeR: UIView!
    
    
    @IBOutlet weak var viewBackgroundChangeR: UIView!
    @IBOutlet weak var lblOpacityR: UILabel!
    
    
    @IBOutlet weak var viewEditting: UIView!
    
    @IBOutlet weak var btnSize: UIButton!
    @IBOutlet weak var btnColor: UIButton!
    @IBOutlet weak var btnFont: UIButton!
    @IBOutlet weak var btnAlignment: UIButton!
    @IBOutlet weak var btnBackground: UIButton!
    
    
    @IBOutlet weak var viewTarget: UIView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var viewBottom: UIView!
    
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var txtName: UILabel!
    
    @IBOutlet weak var lblCopyMessage: UILabel!
    
    var objName = String()
    var objImage = String()
    
    let dropDown = DropDown()
    var arrayPrice: [String] = []
    
    var selectedFontSize : Int = 22
    var selectedFontName : String = "Raleway"
    var selectedColorIndex = 0
    var selectedFontIndex = 0
    
    var selectedBackgrouondPer = 30

    
    let arrColors = [UIColor.white, UIColor.black, UIColor.green,UIColor.yellow, UIColor.red, UIColor.darkGray,UIColor.lightGray, UIColor.gray, UIColor.blue, UIColor.cyan,UIColor.magenta, UIColor.orange, UIColor.purple, UIColor.brown]
    
    
    let fontArray : NSMutableArray = NSMutableArray()
    
    var arrNewFontName = ["Aleo-Bold","AlexBrush-Regular","Antonio-Bold","aAppleTea","AzoftSans-Bold","BebasKai","BlendaScript","Comfortaa-SemiBold","CrimsonText-SemiBoldItalic","Cunia","Raleway-SemiBold","DancingScript-Bold","DephionSlanted","Graduate-Regular","GreatVibes-Regular","HelveticaNeue-Medium","Kavoon-Regular","LimeRock","MarianeLusia","MerriweatherSans-ExtraBold","Oswald-Regular","PlatNomor","Sansation-Bold","Scada-Bold","SpartanMB-Black","Trocchi-Bold","Yesteryear-Regular"]
    
    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 0.0,
                                     bottom: 0.0,
                                     right: 0.0)
    let itemsPerRow: CGFloat = 1
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: 50, height: 35)
        }
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0
        _flowLayout.minimumLineSpacing = 8
        // edit properties here
        
        return _flowLayout
    }
    
    var flowLayout1: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        DispatchQueue.main.async {
            _flowLayout.itemSize = CGSize(width: 85, height: 30)
        }
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        _flowLayout.minimumInteritemSpacing = 0
        _flowLayout.minimumLineSpacing = 2
        // edit properties here
        
        return _flowLayout
    }
    
    var isUnderLine = false
    
    var isUperCase = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtName.font = UIFont.init(name: arrNewFontName[0], size: CGFloat(self.selectedFontSize))
        selectedFontName = arrNewFontName[0]
        
        self.viewBg.alpha = CGFloat((Double(selectedBackgrouondPer)/Double(100)))
        self.lblOpacityR.text = "\(selectedBackgrouondPer)%"

        for family in UIFont.familyNames {
            print("\(family)")
            
            for name in UIFont.fontNames(forFamilyName: family) {
                print("   \(name)")
                fontArray.add(name)
                
            }
        }
        
        collectionViewColor.delegate = self
        collectionViewColor.dataSource = self
        
        collectionViewFonr.delegate = self
        collectionViewFonr.dataSource = self
        
        collectionViewColor.collectionViewLayout = flowLayout
        collectionViewFonr.collectionViewLayout = flowLayout1
        
        
        btnSize0.clipsToBounds = true
        btnSize0.layer.cornerRadius = btnSize0.frame.height / 2
        btnSize0.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner] // Left Corner
        
        btnSize1.clipsToBounds = true
        btnSize1.layer.cornerRadius = btnSize1.frame.height / 2
        btnSize1.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner] // Right Corner
        
        viewEditting.isHidden = true
        btnSize.alpha = 1
        btnColor.alpha = 0.5
        btnFont.alpha = 0.5
        btnAlignment.alpha = 0.5
        btnBackground.alpha = 0.5
        
        viewSizeChangeR.isHidden = false
        viewColorChangeR.isHidden = true
        viewFontChangeR.isHidden = true
        viewAlignmentChangeR.isHidden = true
        viewBackgroundChangeR.isHidden = true
        
        
        self.lblCopyMessage.isHidden = true
        lblCopyMessage.layer.cornerRadius = 12
        lblCopyMessage.clipsToBounds = true
        
        arrayPrice = ["Copy Text","Share as Text","Share as Image"]
        
        self.imgBg.image = UIImage(named: objImage)
        
        setDropDownCountryCode()
        // Do any additional setup after loading the view.
    }
    func setDropDownCountryCode() {
        
        dropDown.dataSource = arrayPrice
        dropDown.anchorView = viewTarget
        dropDown.direction = .any
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            if index == 0 {
                UIPasteboard.general.string = self.txtName.text
                self.view.makeToast("Copy!")

            }
            else if index == 1
            {
                let vc = UIActivityViewController(activityItems: [self.txtName.text!], applicationActivities: nil)
                if UIDevice.current.userInterfaceIdiom == .pad {
                    if let popoverController = vc.popoverPresentationController {
                        let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                        btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                        let barButton = UIBarButtonItem(customView: btnShare)
                        popoverController.barButtonItem = barButton
                    }
                }
                self.present(vc, animated: true)
            }
            else
            {
                let vc = UIActivityViewController(activityItems: [self.imgBg.image!], applicationActivities: nil)
                if UIDevice.current.userInterfaceIdiom == .pad {
                    if let popoverController = vc.popoverPresentationController {
                        let btnShare = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
                        btnShare.center = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height - 50)
                        let barButton = UIBarButtonItem(customView: btnShare)
                        popoverController.barButtonItem = barButton
                    }
                }
                self.present(vc, animated: true)
            }
            
        }
        
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: -viewTarget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.black
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        
        dropDown.reloadAllComponents()
    }
    
    func onChangeBG(type: String, img: UIImage) {
        
        if type != ""
        {
            self.imgBg.image = UIImage(named: type)
        }
        else
        {
            self.imgBg.image = img
        }
    }
    
    //MARK:- Bottom Menu
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func clickedColor(_ sender: Any) {
        viewEditting.isHidden = true
        
    }
    
    @IBAction func clickedTextEdit(_ sender: Any) {
        viewEditting.isHidden = false
        
        viewSizeChangeR.isHidden = false
        viewColorChangeR.isHidden = true
        viewFontChangeR.isHidden = true
        viewAlignmentChangeR.isHidden = true
        viewBackgroundChangeR.isHidden = true

        
        btnSize.alpha = 1
        btnColor.alpha = 0.5
        btnFont.alpha = 0.5
        btnAlignment.alpha = 0.5
        btnBackground.alpha = 0.5
        
    }
    
    @IBAction func clickedShare(_ sender: Any) {
        viewEditting.isHidden = true
        
        self.dropDown.show()
    }
    
    @IBAction func clickedDownlaod(_ sender: Any) {
        viewEditting.isHidden = true
        
        DispatchQueue.main.async {
            
            if let image = self.takeScreenshot() {
                
                self.viewBottom.isHidden = false
                self.viewTop.isHidden = false
                
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
    }
    
    //MARK:- Text Editing
    @IBAction func clickedSize(_ sender: Any) {
        viewSizeChangeR.isHidden = false
        viewColorChangeR.isHidden = true
        viewFontChangeR.isHidden = true
        viewAlignmentChangeR.isHidden = true
        viewBackgroundChangeR.isHidden = true

        btnSize.alpha = 1
        btnColor.alpha = 0.5
        btnFont.alpha = 0.5
        btnAlignment.alpha = 0.5
        btnBackground.alpha = 0.5
    }
    
    @IBAction func clickedColorEditing(_ sender: Any) {
        btnSize.alpha = 0.5
        btnColor.alpha = 1
        btnFont.alpha = 0.5
        btnAlignment.alpha = 0.5
        btnBackground.alpha = 0.5
        
        viewSizeChangeR.isHidden = true
        viewColorChangeR.isHidden = false
        viewFontChangeR.isHidden = true
        viewAlignmentChangeR.isHidden = true
        viewBackgroundChangeR.isHidden = true

        
    }
    
    @IBAction func clickedFont(_ sender: Any) {
        btnSize.alpha = 0.5
        btnColor.alpha = 0.5
        btnFont.alpha = 1
        btnAlignment.alpha = 0.5
        btnBackground.alpha = 0.5
        
        viewSizeChangeR.isHidden = true
        viewColorChangeR.isHidden = true
        viewFontChangeR.isHidden = false
        viewAlignmentChangeR.isHidden = true
        viewBackgroundChangeR.isHidden = true

        
    }
    
    @IBAction func clickedAlignmentEdit(_ sender: Any) {
        btnSize.alpha = 0.5
        btnColor.alpha = 0.5
        btnFont.alpha = 0.5
        btnAlignment.alpha = 1
        btnBackground.alpha = 0.5
        
        viewSizeChangeR.isHidden = true
        viewColorChangeR.isHidden = true
        viewFontChangeR.isHidden = true
        viewAlignmentChangeR.isHidden = false
        viewBackgroundChangeR.isHidden = true

        
    }
    
    @IBAction func clickedBackgroundEdit(_ sender: Any) {
        btnSize.alpha = 0.5
        btnColor.alpha = 0.5
        btnFont.alpha = 0.5
        btnAlignment.alpha = 0.5
        btnBackground.alpha = 1
        
        viewSizeChangeR.isHidden = true
        viewColorChangeR.isHidden = true
        viewFontChangeR.isHidden = true
        viewAlignmentChangeR.isHidden = true
        viewBackgroundChangeR.isHidden = false
        
    }
    
    @IBAction func clickedCancelEditing(_ sender: Any) {
        viewEditting.isHidden = true
    }
    
    //MARK:- Text Name Editing....
    
    @IBAction func clickedMinusSize(_ sender: Any) {
        
        if self.selectedFontSize != 1 {
            self.selectedFontSize =  selectedFontSize - 1
            self.lblChnageFontSize.text = String(self.selectedFontSize)
            self.txtName.font = UIFont.init(name: selectedFontName, size: CGFloat(self.selectedFontSize))
        }
    }
    
    @IBAction func clickedPlusSize(_ sender: Any) {
        if self.selectedFontSize != 50 {
            self.selectedFontSize =  selectedFontSize + 1
            self.lblChnageFontSize.text = String(self.selectedFontSize)
            self.txtName.font = UIFont.init(name: selectedFontName, size: CGFloat(self.selectedFontSize))
        }
    }
    
    @IBAction func clickedBackBack(_ sender: Any) {
        
        if selectedBackgrouondPer != 0 {
            self.selectedBackgrouondPer =  selectedBackgrouondPer - 1
            self.lblOpacityR.text = "\(String(self.selectedBackgrouondPer))%"
            self.viewBg.alpha = CGFloat((Double(selectedBackgrouondPer)/Double(100)))
            print(CGFloat((Double(selectedBackgrouondPer)/Double(100))))
        }
    }
    
    @IBAction func clickedLightLight(_ sender: Any) {
        if selectedBackgrouondPer != 100 {
            self.selectedBackgrouondPer =  selectedBackgrouondPer + 1
            self.lblOpacityR.text = "\(String(self.selectedBackgrouondPer))%"
            self.viewBg.alpha = CGFloat((Double(selectedBackgrouondPer)/Double(100)))
            print(CGFloat((Double(selectedBackgrouondPer)/Double(100))))
        }
    }
    
    
    @IBAction func clickedAlignmentLeft(_ sender: Any) {
        txtName.textAlignment = NSTextAlignment.left
        
    }
    
    @IBAction func clickedAlignmentCenter(_ sender: Any) {
        txtName.textAlignment = NSTextAlignment.center
    }
    
    @IBAction func clickedAlignmentRight(_ sender: Any) {
        txtName.textAlignment = NSTextAlignment.right
    }
    
    @IBAction func clickedUnderLine(_ sender: Any) {
        
        if isUnderLine == true
        {
            isUnderLine = false
           txtName.attributedText = NSAttributedString(string: self.txtName.text!, attributes:
            [.underlineStyle: 0])

        }
        else
        {
            isUnderLine = true
            txtName.attributedText = NSAttributedString(string: self.txtName.text!, attributes:
                [.underlineStyle: NSUnderlineStyle.single.rawValue])
        }
        
    }
    
    @IBAction func clickedUperCase(_ sender: Any) {
        
        if isUperCase == true
        {
            isUperCase = false
            txtName.text = txtName.text?.lowercased()
        }
        else
        {
            isUperCase = true
            txtName.text = txtName.text?.uppercased()
        }
        
    }
    
    
    
    //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    @IBAction func clickedChooseBG(_ sender: Any) {
        viewEditting.isHidden = true
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChooseBgVC") as! ChooseBgVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedFav(_ sender: Any) {
        viewEditting.isHidden = true
        
    }
    
    
    func takeScreenshot(_ shouldSave: Bool = true) -> UIImage? {
        
        self.viewTop.isHidden = true
        self.viewBottom.isHidden = true
        
        var screenshotImage :UIImage?
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale);
        guard let context = UIGraphicsGetCurrentContext() else {return nil}
        layer.render(in:context)
        screenshotImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return screenshotImage
    }
    
    
}

extension QSEditorVC: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collectionViewColor
        {
            return arrColors.count
        }
        else
        {
            return arrNewFontName.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        if collectionView == collectionViewColor
        {
            let cell = collectionViewColor.dequeueReusableCell(withReuseIdentifier: "SelectColorCollectionCell", for: indexPath) as!  SelectColorCollectionCell
            
            cell.mainView.backgroundColor = arrColors[indexPath.row]
            
            cell.mainView.layer.borderColor = UIColor.white.cgColor
            cell.mainView.layer.borderWidth = 1
            
            if selectedColorIndex == indexPath.row {
                cell.imgSeleced.isHidden = false
            }
            else
            {
                cell.imgSeleced.isHidden = true
            }
            
            return cell
        }
        else
        {
            let cell = collectionViewFonr.dequeueReusableCell(withReuseIdentifier: "SelectFontCollectionCell", for: indexPath) as!  SelectFontCollectionCell
            
            cell.lblName.font = UIFont.init(name: arrNewFontName[indexPath.row] as! String, size: CGFloat(14))
            
            if selectedFontIndex == indexPath.row {
                cell.mainView.backgroundColor = UIColor(red: 255/255, green: 160/255, blue: 111/255, alpha: 1)
            }
            else
            {
                cell.mainView.backgroundColor = nil
            }
            
            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collectionViewColor
        {
            self.txtName.textColor = arrColors[indexPath.row]
            
            selectedColorIndex = indexPath.row
            self.collectionViewColor.reloadData()
        }
        else
        {
            self.txtName.font = UIFont.init(name: arrNewFontName[indexPath.row] as! String, size: CGFloat(self.selectedFontSize))
            selectedFontName = arrNewFontName[indexPath.row] as! String
            selectedFontIndex = indexPath.row
            self.collectionViewFonr.reloadData()
        }
        
        
    }
    
}
