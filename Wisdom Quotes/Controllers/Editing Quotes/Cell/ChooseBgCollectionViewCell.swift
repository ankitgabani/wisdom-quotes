//
//  ChooseBgCollectionViewCell.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 11/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ChooseBgCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    
    
}
