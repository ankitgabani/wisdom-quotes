//
//  SelectColorCollectionCell.swift
//  Wisdom Quotes
//
//  Created by Gabani King on 12/06/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class SelectColorCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imgSeleced: UIImageView!
    
}
